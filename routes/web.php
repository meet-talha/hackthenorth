<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function() {
    CRUD::resource('announcement', 'AnnouncementCrudController');
    CRUD::resource('schedule', 'ScheduleCrudController');
});
Route::get('schedule', function() {
    return response()->json(\App\Models\Schedule::all());
});
Route::get('announcements', function(\Illuminate\Http\Request $request) {
    $latest_announcements = \App\Models\Announcement::latest();

    if($request->has('latest_created_time'))
        $latest_announcements = $latest_announcements->where('created_at', '>', $request->input('latest_created_time'));

    $latest_announcements_result = $latest_announcements->get();
    
    return response()->json([
        'list' => $latest_announcements_result,
        'latest_created_time' => $latest_announcements_result->count() > 0 ? $latest_announcements_result->first()->created_at->toDateTimeString() : ($request->has('latest_created_time') ? $request->input('latest_created_time') : null)
    ]);
});
Route::get('/', function () {
    return view('welcome');
});


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);

const hackStart = moment.tz("2018-02-16 22:00", "US/Pacific");
const hackEnd = moment.tz("2018-02-18 10:00", "US/Pacific");

function padTime(t) {
    return t < 10 ? '0' + t : t;
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import EventSchedule from './components/EventSchedule.js';
import VueTimeago from 'vue-timeago';

Vue.use(VueTimeago, {
    name: 'timeago',
    locale: 'en-US',
    locales: {
        'en-US': require('vue-timeago/locales/en-US.json')
    }
});

Vue.component('app', require('./components/AppComponent.vue'));
Vue.component('event-schedule', EventSchedule);
var end;

const root = new Vue({
    el: '#root',
    data: {
        currentTime: moment(),
        endType: null,
    },
    created: function () {
        this.update();
        window.setInterval(this.update, 1000);
    },
    methods: {
        update: function () {
            this.checkAndUpdateEndTime();
            this.currentTime = moment();
        },
        checkAndUpdateEndTime: function() {
            var now = moment();

            if (now.isBefore(hackStart)) {
                end = hackStart;
                this.endType = 'hack_start';
            }
            else {
                end = hackEnd;
                this.endType = now.isBefore(hackEnd) ? 'hack_end' : 'hack_over';
            }
        }
    },
    computed: {
        msLeft: function () {
            return end.diff(this.currentTime);
        },
        hoursLeft: function () {
            return padTime(24 * this.durationLeft.days() + this.durationLeft.hours());
        },
        minutesLeft: function () {
            return padTime(this.durationLeft.minutes());
        },
        secondsLeft: function () {
            return padTime(this.durationLeft.seconds());
        },
        durationLeft: function () {
            return moment.duration(Math.max(0, this.msLeft));
        },
        timeLeft: function () {
            return this.hoursLeft + ':' + this.minutesLeft + ':' + this.secondsLeft;
        }
    }
});

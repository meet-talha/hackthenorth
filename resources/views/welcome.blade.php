<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <base href="{{ url('/') }}">

        <title>TreeHacks 2018</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400|Open+Sans:400,700" rel="stylesheet">

        <!-- App Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- App Vue Container -->
        <div id="root">
            <app
                v-bind:hours-left="hoursLeft"
                v-bind:minutes-left="minutesLeft"
                v-bind:seconds-left="secondsLeft"
                v-bind:time-left="timeLeft"
                v-bind:end-type="endType"
            ></app>
        </div>

        <!-- App Scripts -->
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
